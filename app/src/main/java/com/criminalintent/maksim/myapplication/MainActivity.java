package com.criminalintent.maksim.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button mButtonPush;
    private Button mButtonPol;
    private EditText mEditText;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButtonPush = findViewById(R.id.button_push);
        mButtonPol = findViewById(R.id.button_pop);
        mEditText = findViewById(R.id.edit_text);
        mTextView = findViewById(R.id.text_view);
        final StackByList<String> stringStackByList = new StackByList<>();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_push:
                        stringStackByList.push(mEditText.getText().toString());
                        mTextView.setText(stringStackByList.toString());
                        break;
                    case R.id.button_pop:
                        if (!stringStackByList.isEmpty()) {
                            stringStackByList.pop();
                            mTextView.setText(stringStackByList.toString());
                        } else if (stringStackByList.getTop() == -1) {
                            mTextView.setText("");
                        }
                        break;
                    default:
                        break;
                }
            }
        };
       mButtonPush.setOnClickListener(onClickListener);
       mButtonPol.setOnClickListener(onClickListener);
    }
}
