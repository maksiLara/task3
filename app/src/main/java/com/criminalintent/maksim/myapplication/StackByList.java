package com.criminalintent.maksim.myapplication;

import android.support.annotation.Nullable;

import java.util.ArrayList;

public class StackByList<T> {
    private ArrayList<T> tArrayList;
    private int top;

    public StackByList() {
        tArrayList = new ArrayList<>();
        top = -1;
    }

    boolean isEmpty() {
            return tArrayList.isEmpty();
    }

    void push(T t) {
        int index = ++top;
        tArrayList.add(index, t);
    }

    T pop() {
        int index = top--;
        if (!isEmpty()&&index>-1) {
            return tArrayList.remove(index);
        } else
            return null;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "StackArray=" + tArrayList +
                ", Index=" + top +
                '}';
    }
}
